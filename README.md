# Regex Replaces for Node Sass imports

> Allows you to specify regex replacemens for sass @import paths

## Installation

`npm install node-sass-importer-regex-replace`

## Usage

According to node-sass documentation (https://github.com/sass/node-sass#importer--v200---experimental)
add the importer function to node-sass options

```
const sass = require('node-sass');
const regexReplaces = require('node-sass-importer-regex-replace');

var result = sass.renderSync({
  data: scss_content
  importer: regexReplaces([
    pattern: /^@test\//, replacement: path.resolve('/path/to/folder/')
  ])
});
