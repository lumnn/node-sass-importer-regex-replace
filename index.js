/**
 * Sass importer that replaces import paths starting with @ sign to a folder
 * names.
 *
 * @param  {Object[]} replaces               List of replace configs.
 * @param  {Regex}    replaces[].pattern     Pattern to search for
 * @param  {String}   replaces[].replacement Replacement
 * @return {Function} sass importer
 */
module.exports = function sassImporterRegexReplaceFactory (replaces) {
  if (!Array.isArray(replaces)) {
    throw new Error("argument must be array")
  }

  return function sassImporterRegexReplace (url, prev, done) {
    if (url[0] !== '@') {
      return null;
    }

    replaces.forEach(function (replace) {
        url = url.replace(replace.pattern, replace.replacement)
    })

    return { file: url }
  }
}
